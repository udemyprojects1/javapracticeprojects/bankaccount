package bankAccountApp;

public class Savings extends Account {
    //List properties specific for savings account
    private String safetyDepositBoxID;
    private String safetyDepositBoxKey;

    //Constructor to initialize savings account properties
    public Savings(String name, String sSN, double initDeposit) {
        super(name, sSN, initDeposit);
        accountNumber = "1" + accountNumber;
        setSafetyDepositBox();
    }

    public void setRate() {
        rate = IBaseRate.getBaseRate()-0.25;
    }

    private void setSafetyDepositBox() {
        safetyDepositBoxID =  String.format("%03d", (int)(Math.random()*Math.pow(10,3)));
        safetyDepositBoxKey = String.format("%04d",(int) (Math.random()*Math.pow(10,4)));

    }

    //List methods specific for savings account
    public String toString() {
        return super.toString() + "\nYour Savings Account Features:" +
                "\n Safety Deposit Box ID: " + safetyDepositBoxID +
                "\n Safety Deposit Box Key: " + safetyDepositBoxKey;
    }
}
