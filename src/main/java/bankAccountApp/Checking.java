package bankAccountApp;

public class Checking extends Account {
    //List properties specific for checking account
    private String debitCardNumber;
    private String debitCardPin;

    //Constructor to initialize checking account properties
    public Checking(String name, String sSN, double initDeposit) {
        super(name, sSN, initDeposit);
        accountNumber = "2" + accountNumber;
        setDebitCard();
    }

    public void setRate() {
        rate = IBaseRate.getBaseRate()*0.15;
    }

    private void setDebitCard() {
        debitCardNumber =  String.format("%012d", (long)(Math.random()*Math.pow(10,12)));
        debitCardPin = String.format("%04d",(int) (Math.random()*Math.pow(10,4)));

    }


    //List methods specific for checking account
    public String toString(){
        return super.toString() + "\nYour Checking Account Features" +
                "\n Debit Card Number: " + debitCardNumber +
                "\n Debit Card Pin: " + debitCardPin;
    }
}
