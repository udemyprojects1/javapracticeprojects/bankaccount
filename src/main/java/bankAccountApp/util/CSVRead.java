package bankAccountApp.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CSVRead {
    //This function will read data from CSV file and return as a list
    public static List<String[]> read(String file) {
        List<String[]> data = new ArrayList<>();
        String dataRow;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((dataRow = br.readLine()) != null) {
                String[] dataRecords = dataRow.split(",");
                data.add(dataRecords);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

}
