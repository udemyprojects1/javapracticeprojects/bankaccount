package bankAccountApp;

public abstract class Account implements IBaseRate{
    //List common properties for savings and checking account
    private String name;
    private String sSN;
    private double balance;
    static int index = 10000;
    protected String accountNumber;
    protected double rate;


    //Constructor to set base properties and initialize the account
    public Account(String name, String sSN, double initDeposit) {
        this.name = name;
        this.sSN = sSN;
        balance = initDeposit;

        //set account number
        index++;
        accountNumber = setAccountNumber();
        setRate();
    }

    public abstract void setRate();

    private String setAccountNumber() {
        String lastTwoOfSSN = sSN.substring(sSN.length() - 2);
        int uniqueID = index;
        int randomNumber = (int) (Math.random() * Math.pow(10, 3));
        String formattedRandomNumber = String.format("%03d", randomNumber);

        return lastTwoOfSSN + uniqueID + formattedRandomNumber;
    }

    public void compound(){
        double accruedInterest = Math.round(balance * (rate/100)*100)/100d;
        balance+=accruedInterest;
        System.out.println("Accured Interest: " + accruedInterest + " \u20ac");
        printBalance();
    }

    //List common methods
    public void deposit(double amount){
        balance+=amount;
        System.out.println("Depositing " + amount + " \u20ac");
        printBalance();
    }

    public void withdraw(double amount){
        balance-=amount;
        System.out.println("Withdrawing " + amount + " \u20ac");
        printBalance();
    }

    public void transfer(String toWhere, double amount){
        balance-=amount;
        System.out.println("Transfering " + amount + " \u20ac to " + toWhere);
        printBalance();
    }

    public void printBalance(){
        System.out.println("Your balance is now: " + balance + " \u20ac");
    }

    public String toString(){
        return "Name: " + name +
                "\nAccount number: " + accountNumber +
                "\nBalance: " + balance + " \u20ac" +
                "\nRate: " + rate + "%";
    }



}
