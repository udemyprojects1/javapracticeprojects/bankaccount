package bankAccountApp;

import bankAccountApp.util.CSVRead;

import java.util.ArrayList;
import java.util.List;

public class BankAccountApp {
    public static void main(String[] args) {

        String file = "src/main/resources/NewBankAccounts.csv";

        List<Account> accounts = new ArrayList<>();

        //Read CSV file then create new accounts based on that data
        List<String[]> accountHolders = new ArrayList<>(CSVRead.read(file));
        for (String[] accountHolder : accountHolders) {
            String name = accountHolder[0];
            String sSN = accountHolder[1];
            String accountType = accountHolder[2];
            double initDeposit = Double.parseDouble(accountHolder[3]);
            if (accountType.equals("Savings")) {
                accounts.add(new Savings(name, sSN, initDeposit));

            } else if (accountType.equals("Checking")) {
                accounts.add(new Checking(name, sSN, initDeposit));

            } else {
                System.out.println("Error in opening account.");
            }

        }

        for (Account account: accounts) {
            System.out.println(account);
            System.out.println("\n*********");
        }

    }

}
